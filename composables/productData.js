
const useProductData = () => {
  const productData = useState('product', () => {})
  
  const setProductData = (product) => {
    productData.value = product
  }
  return {
    productData,
    setProductData
  }
}
export default useProductData